const express = require('express');
const sortByDistance = require('sort-by-distance');

const router = express.Router();

const { getRecomendations } = require("../services/personalizeService");
const { searchGoogle } = require("../services/googleService");
const { search, search2, search3, search4, search5, create, create2, remove } = require("../services/elasticService");



router.get('/recommendations/:userId', async (req, res, next) => {
  const userId = req.params.userId;
  const locationIds = await getRecomendations(userId);
  console.log('\nRequested ids\n\n');
  locationIds.map((item) => console.log(item));
  const result = await search(locationIds);
  console.log('\n\nFound ids\n\n');
  result.map(({location}) => console.log(location.id));
  res.send(result);
});

router.get('/personalize/:userId', async (req, res, next) => {
  const userId = req.params.userId;
  const ids = await getRecomendations(userId);
  res.send(ids);
});



router.get('/finaltest', async (req, res, next) => {
  //data from front
  const currentLocation = {lat:55.745509,lon:37.6277462};
  const preferences = 'музей';
  //personalize
  const locationIds = await getRecomendations(preferences);
  // console.log('locationIds',locationIds); 
  //get hashtag and type by location_id in location3
  const locations = await search(locationIds);
  // console.log('locations',locations);
  //get location info by hashtag in location2
  const locationData = await search4(locations);
  // console.log('locationData',locationData); 
  //filter by category
  // const locationDatanew = locationData.filter(location => {
  //   return location.subtype === preferences;
  // })
  // console.log('filter',locationData);

  //get array of locations from locationData
  //

  //sort locations by distance
  // const opts = {
  //   yName: 'lat',
  //   xName: 'lon'
  // }
  
  // const arraylocations = [];

  // locationData.forEach(location => {
  //   arraylocations[] = location.location;
  // })

  // const locations = sortByDistance(currentLocation, arraylocations, opts);
  // console.log('locations',locations);


  //search by hashtag in insta-data-*
  const posts = await search5(locations);
  console.log('listOfphotos',posts);

  locationData.forEach(location => {
    //get type by location.name
    location.subtype = locationData.filter((loc) => {
        if(location.name === loc.name){
          return loc.type;
        }
    });
    location.photo = posts.filter(post => {
        if(location.name === post.name){
          return post.thumbnail_src;
        }
    });
    //get photo from google
    location.mainphoto = 'https://im0-tub-tr.yandex.net/i?id=c044bd5f9edaadbe75acb506f08d5f6b&n=13';//searchGoogle(location.name);
  })
  console.log('locationData',locationData);
  
  // res.send(result);
});





router.get('/test9/:name', async (req, res, next) => {
  const name = req.params.name;
  const google = await searchGoogle(name);
  console.log('google',google);
  res.send(google);
});





router.get('/create', async (req, res, next) => {
  const result = await create();
  res.send(result);
});
router.get('/create2', async (req, res, next) => {
  const result = await create2();
  res.send(result);
});
router.get('/remove', async (req, res, next) => {
  const result = await remove();
  res.send(result);
});
router.get('/test2/:hashtag', async (req, res, next) => {
  const hashtag = req.params.hashtag;
  const result = await search2(hashtag);
  res.send(result);
});
router.get('/test4/:maintag', async (req, res, next) => {
  const maintag = req.params.maintag;
  const result = await search3(maintag);

  console.log(result.length);
  // console.log(Object.keys(result).length);
  // console.log(result[0].display_url)

  res.send(result);
});



module.exports = router;
