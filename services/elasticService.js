process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
const { Client } = require("@elastic/elasticsearch");
const fs = require('fs');

const client = new Client({
  node: "https://ec2-18-184-212-150.eu-central-1.compute.amazonaws.com:9200",
  log: "trace",
  auth: {
    username: "elastic",
    password: "l0StiazaR56sexV3uqOw",
  },
});

//get hashtag and type by location_id in location3
async function search(locationIds = []) {
  const conditions = locationIds.map((id) => ({
    match: {
      // "location.id": id,
      "id": id,
    },
  }));

  const { body } = await client.search(
    {
      index: "locations3",
      // index: "insta_data-*",
      body: {
        query: {
          bool: {
            should: conditions,
          },
        },
      },
    },
    { ignore: [404] }
  );
  console.log('search',body);
  return body.hits.hits.map(({_source}) => _source);
}
//get location info by hashtag in location2
async function search4(locations) {
  const conditions = locations.map((location) => ({
    match: {
      "name": location.name,
    },
  }));

  // console.log('conditions',conditions);

  const { body } = await client.search(
    {
      index: "locations2",
      body: {
        query: {
          bool: {
            should: conditions,
          },
        },
      },
    },
    { ignore: [404] }
  );
  console.log('search',body);
  return body.hits.hits.map(({_source}) => _source);
}
// search by hashtag in insta-data-*
async function search5(locations) {
  const conditions = locations.map((location) => ({
    match: {
      "insta_description": location.hash,
    },
  }));

  const { body } = await client.search(
    {
      index: "insta_data-*",
        body: {
          query: {
            bool: {
              should: conditions,
            },
          },
         //  "query":{  
         //    "query_string":{ 
         //        // "default_field":"Customer", 
         //       "fields":[  
         //          "insta_description"
         //       ],
         //       "query": conditions
         //    }
         // }
       },
    },
    { ignore: [404] }
  );
  console.log('search',body);
  return body.hits.hits.map(({_source}) => _source);
}











//get from insta_data-* by maintag all posts
async function search3(maintag) {

  const { body } = await client.search(
    {
      index: "insta_data-*",
        body: {
          "query":{  
            "query_string":{ 
                // "default_field":"Customer", 
               "fields":[  
                  "insta_description"
               ],
               "query": maintag
            }
         }
       },
    },
    { ignore: [404] }
  );

  console.log(body);
  return body.hits.hits.map(({_source}) => _source);
}




// get from location2 by locationId - maintag
async function search2(hashtag) {
  const { body } = await client.search(
    {
      index: "locations2",
      body: {
        query: {
          "match" : {
              "maintag" : {
                  "query" : hashtag
              }
          }
          // "match_all": {} - get all
        },
      },
    },
    { ignore: [404] }
  );
  console.log(body);
  return body.hits.hits.map(({_source}) => _source);
}





async function update(maintag) {
  const { body } = await client._update_by_query(
    {
      index: "locations2",
        body: {
          "query":{  
            "query_string":{ 
               "fields":[  
                  "hash"
               ],
               "query": maintag
            }
         }
       },
    },
    { ignore: [404] }
  );
}

async function remove(hashtag) {
  const { body } = await client.deleteByQuery(
    {
      index: "locations2",
      body: {
        query: {
          "match_all": {} 
        },
      },
    },
    { ignore: [404] }
  );
  console.log(body);
}
async function create() {
    const data = JSON.parse(fs.readFileSync(__dirname + '/myfile.json'));
    
    for (let i = 1; i < data.length; i++ ) {
      // await client.create({
      await client.index({
          //   refresh: true,
        id: i,
        index: "locations2",
        type : "_doc",
        pretty : true,
        body: data[i]
      },
      function(error, response) {
        if (error) {
          console.error("Failed to import data", error);
          return;
        }
        else {
          console.log("Successfully imported data", data[i]);
        }
      });
    }
}
async function create2() {
    const data = JSON.parse(fs.readFileSync(__dirname + '/myfiletype.json'));
    
    for (let i = 1; i < data.length; i++ ) {
      // await client.create({
      await client.index({
          //   refresh: true,
        id: i,
        index: "locations3",
        type : "_doc",
        pretty : true,
        body: data[i]
      },
      function(error, response) {
        if (error) {
          console.error("Failed to import data", error);
          return;
        }
        else {
          console.log("Successfully imported data", data[i]);
        }
      });
    }
}

module.exports = { 
  search,
  search2,
  search3,
  search4,
  search5,
  remove,
  create,
  create2
}







